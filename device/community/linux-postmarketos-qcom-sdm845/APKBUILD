# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Co-Maintainer: Joel Selvaraj <jo@jsfamily.in>
# Stable Linux kernel with patches for SDM845 devices
# Kernel config based on: arch/arm64/configs/defconfig and sdm845.config

_flavor="postmarketos-qcom-sdm845"
pkgname=linux-$_flavor
pkgver=5.16.2
pkgrel=1
pkgdesc="Mainline Kernel fork for SDM845 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-zram
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-iwd"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="linux"
_config="config-$_flavor.$arch"
_tag="sdm845-5.16.2"

# Source
source="
	$_repo-$_tag.tar.gz::https://gitlab.com/sdm845-mainline/$_repo/-/archive/$_tag/$_repo-$_tag.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
ebf59f9572873b10f0321c7464e20be7715953db9be36512da38e95adaf1846de180565bdfbdf660a3e6ab4339e40bdd45f67505cf71adec46a439b0d3c8eb32  linux-sdm845-5.16.2.tar.gz
f855bb9157779f11dc2597a0fa1f6da6822f4d9c716480458ecbdf747ead3671df34d6273d79de78703942d97f774db1b18fe1ba5e5ed397397f844531f950af  config-postmarketos-qcom-sdm845.aarch64
"
